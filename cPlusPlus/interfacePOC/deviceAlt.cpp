/*

*/

#include <stdio.h>
#include <vector>
#include <map>
#include <string>
#include <utility>

class IComms //comms interface
{
    public:
        virtual ~ IComms() {printf("IComms: destructor\n");};
        virtual bool connect() = 0;
        virtual bool disconnect() = 0;
};

class TcpMethods : public IComms //implements IComms
{
    public:
        virtual ~ TcpMethods() {printf("TcpMethods: destructor\n");};
        bool connect() override
        {
            printf("TcpMethods: connect\n");
            return true;
        };
        bool disconnect() override
        {
            printf("TcpMethods: disconnect\n");
            return true;
        };
};

class NtdsMethods : public IComms //implements IComms
{
    public:
        virtual ~ NtdsMethods() {printf("NtdsMethods: destructor\n");};
        bool connect() override
        {
            printf("NtdsMethods: connect\n");
            return true;
        };
        bool disconnect() override
        {
            printf("NtdsMethods: disconnect\n");
            return true;
        };
};

class iDevice //device interface
{
    public:
        virtual ~ iDevice() {printf("iDevice: destructor\n");};
        virtual bool start() = 0;
        virtual bool stop() = 0; 
};

class CommonDeviceMethods
{
    public:
        CommonDeviceMethods();
        CommonDeviceMethods(std::string commType)
        {
            if(commType == "tcp")
            {
                m_comm = new TcpMethods();
            }
            else if(commType == "ntds")
            {
                m_comm = new NtdsMethods();
            }
            else
            {
                //error
            }
            printf("CommonDeviceMethods: constructor\n");
        }
        virtual ~ CommonDeviceMethods()
        {
            delete m_comm;
            m_comm = nullptr;
            printf("CommonDeviceMethods: destructor\n");
        }

        IComms* m_comm;
};

class ClassADevice : public iDevice, private CommonDeviceMethods //implements iDevice, extends CommonDeviceMethods
{
    public:
        ClassADevice();
        ClassADevice(std::string commType) : CommonDeviceMethods{commType}
        {
            printf("ClassADevice: constructor\n");
        }
        virtual ~ ClassADevice() {printf("ClassADevice: destructor\n");}
        bool start() override 
        {
            printf("ClassADevice: start()\n");
            m_comm->connect();
            return true;
        }
        bool stop() override
        {
            printf("ClassADevice: stop()\n");
            m_comm->disconnect();
            return true;
        }

};

class ClassBDevice : public iDevice, private CommonDeviceMethods //implements iDevice, extends CommonDeviceMethods
{
    public:
        ClassBDevice(std::string commType) : CommonDeviceMethods{commType}
        {
            printf("ClassBDevice: constructor\n");
        }
        virtual ~ ClassBDevice() {printf("ClassBDevice: destructor\n");}
        bool start() override 
        {
            printf("ClassBDevice: start()\n");
            m_comm->connect();
            return true;
        }
        bool stop() override
        {
            printf("ClassBDevice: stop()\n");
            m_comm->disconnect();
            return true;
        }
};

std::string TAG_CLASS_A = "classA";
std::string TAG_CLASS_B = "classB";
typedef std::pair <std::string,std::string> device;
device A("classA", "tcp");
device B("classA", "ntds");
device C("classB", "tcp");
std::vector<device> configDevices = {A, B, C};

int main(void)
{
    std::map<std::string, std::vector<iDevice*>> mDevices;
    for(auto i : configDevices)
    {
        if(i.first == TAG_CLASS_A)
        {
            mDevices[TAG_CLASS_A].push_back(new ClassADevice(i.second));
        }
        else if(i.first == TAG_CLASS_B)
        {
            mDevices[TAG_CLASS_B].push_back(new ClassBDevice(i.second));
        }
        else
        {
            //invalid
        }  
    }

    for(auto i : mDevices)
    {
        for(auto j : i.second)
        {
            j->start();
            j->stop();
            delete(j);
            j = nullptr;
        }
    }
    
    return 0;
}