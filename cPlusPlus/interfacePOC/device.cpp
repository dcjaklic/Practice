/*

*/

#include <stdio.h>
#include <vector>
#include <map>
#include <string>

class iDevice
{
    public:
        virtual ~ iDevice() {printf("iDevice: destructor\n");};
        virtual bool start() = 0;
        virtual bool stop() = 0; 
};

class CommonCommMethods
{
    public:
        virtual ~ CommonCommMethods() {printf("CommonCommMethods: destructor\n");};
        void preprocessThings ()
        {
            printf("CommonCommMethods: preprocessThings\n");
        }
        virtual bool connect() = 0;
        virtual bool disconnect() = 0;
};

class TcpMethods : public CommonCommMethods //extends CommonCommMethods
{
    public:
        virtual ~ TcpMethods() {printf("TcpMethods: destructor\n");};
        bool connect() override
        {
            preprocessThings();
            printf("TcpMethods: connect\n");
            return true;
        };
        bool disconnect() override
        {
            printf("TcpMethods: disconnect\n");
            return true;
        };
};

class NtdsMethods : public CommonCommMethods
{
    public:
        virtual ~ NtdsMethods() {printf("NtdsMethods: destructor\n");};
        bool connect() override
        {
            printf("NtdsMethods: connect\n");
            return true;
        };
        bool disconnect() override
        {
            printf("NtdsMethods: disconnect\n");
            return true;
        };
};

class ClassADevice : public iDevice, private TcpMethods
{
    public:
        ClassADevice()
        {
            printf("ClassADevice: constructor\n");
        }
        virtual ~ ClassADevice() {printf("ClassADevice: destructor\n");}
        bool start() override 
        {
            printf("ClassADevice: start()\n");
            connect();
            return true;
        }
        bool stop() override
        {
            printf("ClassADevice: stop()\n");
            disconnect();
            return true;
        }
};

class ClassBDevice : public iDevice, private NtdsMethods
{
    public:
        ClassBDevice() { printf("ClassBDevice: constructor\n");}
        virtual ~ ClassBDevice() {printf("ClassBDevice: destructor\n");}
        bool start() override 
        {
            printf("ClassBDevice: start()\n");
            connect();
            return true;
        }
        bool stop() override
        {
            printf("ClassBDevice: stop()\n");
            disconnect();
            return true;
        }
};

std::string TAG_CLASS_A = "classA";
std::string TAG_CLASS_B = "classB";
std::vector<std::string> configDevices = {"classA", "classA", "classB"};

int main(void)
{
    std::map<std::string, std::vector<iDevice*>> mDevices;
    for(auto i : configDevices)
    {
        if(i == TAG_CLASS_A)
        {
            mDevices[TAG_CLASS_A].push_back(new ClassADevice());
        }
        else if(i == TAG_CLASS_B)
        {
            mDevices[TAG_CLASS_B].push_back(new ClassBDevice());
        }
        else
        {
            //invalid
        }  
    }

    for(auto i : mDevices)
    {
        for(auto j : i.second)
        {
            j->start();
            j->stop();
            delete(j);
            j = nullptr;
        }
    }
    
    return 0;
}