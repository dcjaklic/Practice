#include "mTree.h"

mTree::mTree()
{
    mRoot = nullptr;
}

mTree::mTree(int x)
{
    mRoot->data = x;
}

mTree::~mTree()
{
    
}

Node* mTree::mInsert(int x, Node *n)
{
    if(n == nullptr)
    {
        n = new Node;
        n->data = x;
        n->left = n->right = nullptr;
    }
    else if(x < n->data)
    {
        n->left = mInsert(x, n->left);
    }
    else //x >= n->data
    {
        n->right = mInsert(x, n->right);
    }
    
    return n;
}

void mTree::mInsert(int x)
{
    mRoot = mInsert(x, mRoot);
}

void mTree::mInOrder(Node *n)
{
    if(n == nullptr) return;
    mInOrder(n->left);
    printf("%i ", n->data);
    mInOrder(n->right);
}

void mTree::mPrint()
{
    mInOrder(mRoot);
    printf("\n");
}