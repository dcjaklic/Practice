#include <stdio.h>
#include "mTree.h"

//https://www.cs.bu.edu/teaching/cpp/writing-makefiles/
//best makefile article of all time ^

int main()
{
    mTree t;
    t.mInsert(9);
    t.mInsert(5);
    t.mInsert(2);
    t.mInsert(4);
    t.mInsert(11);
    t.mPrint();
    // looking for 2, 4, 5, 9, 11

    return 0;
}