#include <stdio.h>

struct Node
{ 
    int data; 
    Node *left, *right; 
};

class mTree
{
    private:
    
    Node *mRoot;
    Node* mInsert(int x, Node *n);
    void mInOrder(Node *n);
    
    public:
    
    mTree();
    mTree(int x);
    ~mTree();
    void mInsert(int x);
    void mPrint();
    
};