#include <cstdio>
#include <string>
#include <stack>

bool isParenthesisMatch(const char &open, const char &close)
{
	if(open == '(' && close == ')') return true;
	else if(open == '{' && close == '}') return true;
	else if(open == '[' && close == ']') return true;
	return false;
}

bool isBalancedParenthesis(const std::string &input)
{
    std::stack<char> m_lifo;
    for(auto const &i : input)
    {
        std::printf("%c", i);
        if(i == '(' || i == '{' || i == '[')
        {
            m_lifo.push(i);
        }
        else if (i == ')' || i == '}' || i == ']')
        {
            if(m_lifo.empty() || !isParenthesisMatch(m_lifo.top(), i))
            {
                return false;
            }
            else
            {
                m_lifo.pop();
            }
        }
    }
    return m_lifo.empty() ? true : false;
}

int main()
{
    std::printf(isBalancedParenthesis("{[()]}") ? " balanced\n" : " not balanced\n");
    std::printf(isBalancedParenthesis("{()]}") ? " balanced\n" : " not balanced\n");

    return 0;
}