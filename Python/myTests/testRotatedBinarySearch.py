import unittest
import random
from sortAndSearch.rotateBinarySearch import myRotatedBinarySearch

class binarySearchTest1(unittest.TestCase):
    def setUp(self):
        self.list1 = list()

    # empty list, return 'None'
    def testEmptyList(self):
        result = myRotatedBinarySearch(self.list1, 5)
        print(self.list1)
        self.assertEqual(result, None)
    
    # not found, return 'None'
    def testNumberNotInList(self):
        for i in range(0, 10):
            self.list1.insert(i, random.randrange(0, 100, 2))
        self.list1.sort()
        result = myRotatedBinarySearch(self.list1, 45)
        print(self.list1)
        self.assertEqual(result, None)

    def testNumberInList(self):
        for i in range(0, 10):
            self.list1.insert(i, i*3)
            # 0, 3, 6, .. 27
        self.list1.sort()
        result = myRotatedBinarySearch(self.list1, 15)
        print(self.list1)
        self.assertEqual(result, 5)


if __name__ == '__main__':
    unittest.main()