#python3 -m unittest -v myTests/testLinkedListStrCmp.py

import unittest
from linkedList.linkedList import Node, LinkedList
from linkedList.linkedListStrCmp import linkedListStrCmp

class linkedListStrCmpTest1(unittest.TestCase):
  
    def setUp(self):
        self.myList1 = LinkedList()
        self.myList2 = LinkedList()

    def test0(self):
        result = linkedListStrCmp(self.myList1, self.myList2)
        self.assertEqual(result, 0)

    def test1(self):
        self.myList1.insert(Node('d'))
        self.myList1.insert(Node('a'))
        self.myList1.insert(Node('n'))
        result = linkedListStrCmp(self.myList1, self.myList2)
        self.assertEqual(result, 1)

    def test2(self):
        self.myList2.insert(Node('d'))
        self.myList2.insert(Node('a'))
        self.myList2.insert(Node('n'))
        result = linkedListStrCmp(self.myList1, self.myList2)
        self.assertEqual(result, -1)

    def test3(self):
        self.myList1.insert(Node('d'))
        self.myList1.insert(Node('e'))
        self.myList1.insert(Node('n'))
        self.myList2.insert(Node('d'))
        self.myList2.insert(Node('a'))
        self.myList2.insert(Node('n'))
        result = linkedListStrCmp(self.myList1, self.myList2)
        self.assertEqual(result, 1)
    
    def test4(self):
        self.myList1.insert(Node('b'))
        self.myList2.insert(Node('a'))
        self.myList2.insert(Node('b'))
        self.myList2.insert(Node('c'))
        result = linkedListStrCmp(self.myList1, self.myList2)
        self.assertEqual(result, 1)

    def test5(self):
        self.myList1.insert(Node('d'))
        self.myList1.insert(Node('j'))
        self.myList2.insert(Node('d'))
        self.myList2.insert(Node('j'))
        result = linkedListStrCmp(self.myList1, self.myList2)
        self.assertEqual(result, 0)

if __name__ == '__main__':
    unittest.main()
