import unittest
from linkedList.linkedList import Node, LinkedList
from linkedList.linkedListAddLists import linkedListAddLists

class linkedListAddListsTest1(unittest.TestCase):
  
    def setUp(self):
        self.myList1 = LinkedList()
        self.myList2 = LinkedList()
        self.result = LinkedList()

    #test imput of 2 empty lists
    def test0(self):
        linkedListAddLists(self.myList1, self.myList2, self.result)
        self.assertEqual(self.result.first, None)

    #test imput 1 empty list
    def test1(self):
        # 123 + 0 = 123
        self.myList1.insert(Node(1))
        self.myList1.insert(Node(2))
        self.myList1.insert(Node(3))
        linkedListAddLists(self.myList1, self.myList2, self.result)
        
        iterator = self.result.first
        self.assertEqual(iterator.value, 1)

        iterator = iterator.next
        self.assertEqual(iterator.value, 2)
        
        iterator = iterator.next
        self.assertEqual(iterator.value, 3)
        
        #ensure correct length
        iterator = iterator.next
        self.assertEqual(iterator, None)

    #test sum of two lists with result of same length
    def test2(self):
        # 32 + 41 = 73
        self.myList1.insert(Node(3))
        self.myList1.insert(Node(2))
        self.myList2.insert(Node(4))
        self.myList2.insert(Node(1))
        linkedListAddLists(self.myList1, self.myList2, self.result)

        iterator = self.result.first
        self.assertEqual(iterator.value, 7)

        iterator = iterator.next
        self.assertEqual(iterator.value, 3)
        
        #ensure correct length
        iterator = iterator.next
        self.assertEqual(iterator, None)

    #test sum of two lists with result of same length
    def test3(self):
        # 52 + 49 = 101
        self.myList1.insert(Node(5))
        self.myList1.insert(Node(2))
        self.myList2.insert(Node(4))
        self.myList2.insert(Node(9))
        linkedListAddLists(self.myList1, self.myList2, self.result)

        iterator = self.result.first
        self.assertEqual(iterator.value, 1)

        iterator = iterator.next
        self.assertEqual(iterator.value, 0)

        iterator = iterator.next
        self.assertEqual(iterator.value, 1)
        
        #ensure correct length
        iterator = iterator.next
        self.assertEqual(iterator, None)

if __name__ == '__main__':
    unittest.main()
