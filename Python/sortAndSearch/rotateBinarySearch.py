#-----------------------------------------------------
#File:      rotatedBinarySearch.py
#Task:      Implement a binary search of rotated sorted array.
#           Can't use any built-in search/sort functions.
#           Return 0 based index.
#           Keep at log(n) TC. (Can't find pivot w/ for loop)
#Author:    DJ
#Date:      03/28/20
#Notes:     Time Complexity = log(n) = good
#https://www.geeksforgeeks.org/top-10-algorithms-in-interview-questions/
#------------------------------------------------------

def myRotatedBinarySearch(m_list, key):
    # empty list, return 'None'
    if(len(m_list) == 0):
        return None

#end myRotatedBinarySearch
