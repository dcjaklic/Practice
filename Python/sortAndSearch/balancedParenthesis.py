def isParenthesisMatch(open, close):
    if(open == '(' and close == ')'):
        return True
    elif(open == '{' and close == '}'):
        return True
    elif(open == '[' and close == ']'):
        return True
    else:
        return False


def isBalancedParenthesis(input):
    m_lifo = []
    for i in input:
        print(i)
        if(i == '(' or i == '{' or i == '['):
            m_lifo.append(i)
        elif (i == ')' or i == '}' or i == ']'):
            top = len(m_lifo)-1
            if(not m_lifo or not isParenthesisMatch(m_lifo[top], i)):
                return False
            else:
                m_lifo.pop()
    return True if not m_lifo else False


print("balanced\n" if isBalancedParenthesis("{[()]}") else "not balanced\n")
print("balanced\n" if isBalancedParenthesis("{[()}") else "not balanced\n")