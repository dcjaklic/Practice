#-----------------------------------------------------
#File:      binarySearch.py
#Task:      Implement a binary search of sorted array.
#           Can't use any built-in search/sort functions.
#           Return 0 based index.
#Author:    DJ
#Date:      01/23/20
#Notes:     Time Complexity = log(n) = good
#https://www.geeksforgeeks.org/top-10-algorithms-in-interview-questions/
#------------------------------------------------------

def binarySearchHelper(m_list, key, leftInd, rightInd):
    if(leftInd > rightInd):
        return None
    
    midPoint = int((leftInd+rightInd)/2)
    if(m_list[midPoint] == key):
        return midPoint
    elif(key > m_list[midPoint]):
        binarySearchHelper(m_list, key, midPoint+1, rightInd)
    elif(key < m_list[midPoint]):
        binarySearchHelper(m_list, key, leftInd, midPoint-1)
    else:
        return None
#end binarySearchHelper

def myBinarySearch(m_list, key):
    # empty list, return 'None'
    if(len(m_list) == 0):
        return None

    return binarySearchHelper(m_list, key, 0, (len(m_list)-1))
#end myBinarySearch
