class Node:
    def __init__(self, val):
        self.left = None
        self.right = None
        self.data = val

def insert(root, newNode):
    if(not root):
        root = newNode
    else:
        if(newNode.data < root.data):
            if root.left == None:
                root.left = newNode
            else:
                insert(root.left, newNode)
        else:
            if root.right == None:
                root.right = newNode
            else:
                insert(root.right, newNode)

def insert2(node, newValue):
    if(not node):
        node = Node(newValue)
    else:
        if(newValue < node.data):
            node.left = insert2(node.left, newValue)
        else:
            node.right = insert2(node.right, newValue)
    return node

def findMin(node):
    if node == None:
        return None
    elif node.left == None:
        return node
    else:
        return findMin(node.left)
    
def findMax(node):
    if node == None:
        return None
    elif node.right == None:
        return node
    else:
        return findMax(node.right)
    
def delete(node, value):
    #post order
    if node == None:
        return None
    elif(value < node.data):
        node.left = delete(node.left, value)
    elif(value > node.data):
        node.right = delete(node.right, value)
    else: # found value to remove
        tempNode = None
        if(node.left and node.right): # node has 2 children
            tempNode = findMin(node.right)
            node.data = tempNode.data
            node.right = delete(node.right, node.data)
        else: # node has 0/1 children
            if(node.left == None):
                node = node.right
            elif(node.right == None):
                node = node.left
    return node
    
def deleteAll(node):
    #post order
    if node == None:
        return None
    deleteAll(node.left)
    deleteAll(node.right)
    node.left = None
    node.right = None
    node.data = None
    node = None
   
def printTree(tree):
	if tree:
		printTree(tree.left)
		print(tree.data)
		printTree(tree.right)
#
		
def printTreeDetailed(tree):
    rootExists = False
    if tree:
        printTreeDetailed(tree.left)
	    leftData = tree.left.data if tree.left else "-"
		rightData = tree.right.data if tree.right else "-"
		print(tree.data, "address:", hex(id(tree)),
		    "left", leftData, hex(id(tree.left)),
		    "right", rightData, hex(id(tree.right)))
		printTreeDetailed(tree.right)
		rootExists = True
		
	if not rootExists:
        print("empty")
#

m_tree = Node(9)
insert2(m_tree,5)
insert2(m_tree,2)
insert2(m_tree,4)
insert2(m_tree,11)
printTreeDetailed(m_tree)
print("max", findMax(m_tree).data, "min", findMin(m_tree).data)
delete(m_tree, 9)
printTreeDetailed(m_tree)
deleteAll(m_tree)
printTreeDetailed(m_tree)
#looking for 2, 4, 5, 9, 11
#   5
#  4 9
# 2   11
