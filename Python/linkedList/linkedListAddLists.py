#-----------------------------------------------------
#File:      linkedListAddLists.py
#Task:      Given two numbers represented by two linked 
#           lists, write a function that returns the sum list. 
#           The sum list is linked list representation of the 
#           addition of two input numbers. It is not allowed 
#           to modify the lists. Also, not allowed to use 
#           explicit extra space
#Author:    DJ
#Date:      01/23/20
#------------------------------------------------------

from linkedList.linkedList import Node

#string approach
def linkedListAddLists(llist1, llist2, resultList):
    iterator1 = llist1.first
    iterator2 = llist2.first

    #early returns for special conditions
    # 1) both lists empty
    # 2) Not Implemented - could check for 'result' to be empty
    if(iterator1 == None and iterator2 == None):
        return
    
    firstNumberString = ""
    secondNumberString = ""
    while(iterator1 != None):
        firstNumberString += str(iterator1.value)
        iterator1 = iterator1.next
    while(iterator2 != None):
        secondNumberString += str(iterator2.value)
        iterator2 = iterator2.next
    if(firstNumberString == ""): firstNumberString = 0
    if(secondNumberString == ""): secondNumberString = 0

    # result in number form
    result = int(firstNumberString) + int(secondNumberString)
    # convert to string
    result = str(result)

    for c in result:
        resultList.insert(Node(int(c)))

#end linkedListAddLists
