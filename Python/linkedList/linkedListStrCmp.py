#-----------------------------------------------------
#File:      linkedListStrCmp.py
#Task:      Given two strings, represented as linked 
#           lists (every character is a node in a linked 
#           list). Write a function compare() that works 
#           similar to strcmp(), i.e., it returns 0 if 
#           both strings are same, 1 if first linked list 
#           is lexicographically greater, and -1 if the 
#           second string is lexicographically greater.
#Author:    DJ
#Date:      01/22/20
#------------------------------------------------------

#assume all lowercase
#assume valid input
def linkedListStrCmp(llist1, llist2):
    iterator1 = llist1.first
    iterator2 = llist2.first
    while(iterator1 != None or iterator2 != None):
        if(iterator1 == None):
            return -1
        if(iterator2 == None):
            return 1
        if(iterator1.value > iterator2.value):
            return 1
        if(iterator2.value > iterator1.value):
            return -1
        iterator1 = iterator1.next
        iterator2 = iterator2.next
    else:
        return 0
#end linkedListStrCmp
