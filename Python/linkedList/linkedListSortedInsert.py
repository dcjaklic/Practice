#-----------------------------------------------------
#File:      linkedListSortedInsert.py
#Task:      Implement a sorted linked list insert
#           function.
#Author:    DJ
#Date:      01/17/21
#------------------------------------------------------
from linkedList.linkedListBasicInsert import linkedListBasicInsert

def linkedListSortedInsert(llist, newNode):
    #Corner case 1: Empty list
    if(llist.first == None):
        linkedListBasicInsert(llist, newNode)
        return
    #Corner case 2: New first node
    if(llist.first.value > newNode.value):
        newNode.next = llist.first
        llist.first = newNode
        return
    iterator = llist.first
    while(iterator.next != None):
        if(iterator.next.value > newNode.value):
            break
        iterator = iterator.next   
    newNode.next = iterator.next
    iterator.next = newNode
#end linkedListSortedInsert