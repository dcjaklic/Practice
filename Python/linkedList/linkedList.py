#-----------------------------------------------------
#File:      linkedList.py
#Task:      Implement a basic linked list with no
#           addtional Python libraries.
#Author:    DJ
#Date:      01/17/20
#https://www.geeksforgeeks.org/top-10-algorithms-in-interview-questions/
#------------------------------------------------------

from linkedList.linkedListBasicInsert import linkedListBasicInsert
from linkedList.linkedListSortedInsert import linkedListSortedInsert
from linkedList.linkedListDelete import linkedListDelete

class Node:
    value = 0
    next = None

    def __init__(self):
        self.data = []

    def __init__(self, value):
        self.value = value
#end class Node

class LinkedList:
    first = None

    def __init__(self):
        self.data = []

    def print(self):
        iterator = self.first
        while(iterator != None):
            print(iterator.value)
            iterator = iterator.next

    def printDetailed(self):
        iterator = self.first
        firstAdress = hex(id(self.first))
        print("FIRST points to: " + str(firstAdress))
        while(iterator != None):
            selfAdress = hex(id(iterator))
            nextAddress = hex(id(iterator.next))
            print("Node Adress: " + str(selfAdress) + ", Node Value: " + str(iterator.value) + ", Next Address: " + str(nextAddress))
            iterator = iterator.next
        print("")      

    def insert(self, newNode):
        linkedListBasicInsert(self, newNode)

    def insertSorted(self, newNode):
        linkedListSortedInsert(self, newNode)

    def delete(self, deleteNode):
        linkedListDelete(self, deleteNode)
#end class LinkedList

#main
"""
myList1 = LinkedList()
myList2 = LinkedList()

myNode0 = Node('h')
myNode1 = Node('e')
myNode2 = Node('l')
myNode3 = Node('l')
myNode4 = Node('o')

myList1.insert(myNode0)
myList1.insert(myNode1)
myList1.insert(myNode2)
myList1.insert(myNode3)

myNode5 = Node('h')
myNode6 = Node('e')
myNode7 = Node('l')
myNode8 = Node('l')
myNode9 = Node('l')

myList2.insert(myNode5)
myList2.insert(myNode6)
myList2.insert(myNode7)

myList1.print()
myList2.print()

result = linkedListStrCmp(myList1, myList2)
print("linked list comp: " + str(result))
"""

