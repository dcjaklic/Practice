#-----------------------------------------------------
#File:      linkedListDelete.py
#Task:      Implement a linked list delete function.
#Author:    DJ
#Date:      01/17/20
#------------------------------------------------------

def linkedListDelete(llist, nodeToDelete):
    if(nodeToDelete == llist.first):
        llist.first = llist.first.next
    else:
        iterator = llist.first
        while(iterator.next != nodeToDelete):
            iterator = iterator.next
        iterator.next = nodeToDelete.next
#end linkedListDelete
