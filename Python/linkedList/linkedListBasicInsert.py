#-----------------------------------------------------
#File:      linkedListBasicInsert.py
#Task:      Implement a basic linked list insert
#           function (no sorting).
#Author:    DJ
#Date:      01/17/20
#------------------------------------------------------

def linkedListBasicInsert(llist, newNode):
    if(llist.first == None):
        llist.first = newNode
        newNode.next = None
        return
    iterator = llist.first
    while(iterator.next != None):
        iterator = iterator.next
    iterator.next = newNode
    newNode.next = None
#end linkedListBasicInsert